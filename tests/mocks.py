"""Test mocks."""
from unittest.mock import NonCallableMock
from uuid import uuid4

import datawarehouse

MOCK_CHECKOUT_ID = "redhat:{unique}"
MOCK_BUILD_ID = "redhat:{unique}-x86_64-kernel"
MOCK_TEST_ID = "redhat:{unique}-x86_64-kernel_upt_1"
MOCK_TESTRESULT_ID = "redhat:{unique}-x86_64-kernel_upt_1.5"
MOCK_ISSUE_ID = 4321


def get_mocked_checkout(*, id=None, **kwargs):
    """Return a kcidb checkout mock."""
    if id is None:
        id = MOCK_CHECKOUT_ID.format(unique=uuid4().hex)
    kwargs.setdefault("misc", {})
    kwargs.setdefault("log_url", None)
    return NonCallableMock(name=f"checkout-{id}", id=id, **kwargs, spec=["all"])


def get_mocked_build(*, checkout_id, id=None, **kwargs):
    """Return a kcidb build mock."""
    if id is None:
        id = MOCK_BUILD_ID.format(unique=uuid4().hex)
    kwargs.setdefault("log", None)
    kwargs.setdefault("architecture", None)
    kwargs.setdefault("variant", None)
    kwargs.setdefault("misc", {})
    return NonCallableMock(
        name=f"build-{id}", id=id, checkout_id=checkout_id, **kwargs, spec=["boots"]
    )


def get_mocked_test(*, build_id, id=None, **kwargs):
    """Return a kcidb test mock."""
    if id is None:
        id = MOCK_TEST_ID.format(unique=uuid4().hex)
    kwargs.setdefault("comment", "Test something")
    kwargs.setdefault("waived", None)
    return NonCallableMock(name=f"test-{id}", id=id, build_id=build_id, **kwargs, spec=[])


def get_mocked_test_results(*, test_id, id=None, **kwargs):
    """Return a mock of a KCIDBTestResult."""
    if id is None:
        id = MOCK_TESTRESULT_ID.format(unique=uuid4().hex)
    return NonCallableMock(name=f"testresult-{id}", id=id, test_id=test_id, **kwargs, spec=[])


def get_mocked_issue_occurrence(issue_attributes=None, **kwargs):
    """Return a mock of an issue_occurrence."""
    ids = {
        id_key: id_value
        for id_key in ("checkout_id", "build_id", "test_id", "testresult_id")
        if (id_value := kwargs.setdefault(id_key, None))
    }
    assert len(ids) == 1, "Exactly one ID must be defined"

    if issue_attributes is None:
        issue_attributes = {}

    kwargs.setdefault("is_regression", False)
    return NonCallableMock(name=f"occurrence-{ids}", issue=issue_attributes, **kwargs)


def get_mocked_kcidbfile(
    checkouts=None, builds=None, tests=None, results=None, issueoccurrences=None
):
    """Return a mock of a KCIDBFile."""
    if checkouts is None:
        checkouts = []
    if builds is None:
        builds = []
    if tests is None:
        tests = []
    if results is None:
        results = []
    if issueoccurrences is None:
        issueoccurrences = []

    return NonCallableMock(
        name="kcidbfile",
        checkouts=checkouts,
        builds=builds,
        tests=tests,
        testresults=results,
        issueoccurrences=issueoccurrences,
        spec=datawarehouse.objects.KCIDBFile,
    )
