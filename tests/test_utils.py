import copy
import textwrap
from unittest import TestCase
from unittest.mock import MagicMock

import responses

from reporter.data import CheckoutData
from reporter.utils import LOGGER
from reporter.utils import add_cross_references
from reporter.utils import add_testresults_to_kcidbfile_dict
from reporter.utils import get_architecture_plus_variant
from reporter.utils import get_artifacts
from reporter.utils import get_attributes
from reporter.utils import join_addresses
from reporter.utils import status_to_emoji
from reporter.utils import to_json
from reporter.utils import unique_test_descriptions
from reporter.utils import yesno

from . import mocks


class TestUtils(TestCase):
    """Tests for reporter/utils.py ."""

    def test_add_testresults_key_to_kcidbfile_dict(self):
        """This test ensures that testresults key (list) is attached to the kcidb_file.data
        testresults are needed for later processing by TestData
        Attachment of test_id key to each testresult is also verified.
        """
        kcidb_file = MagicMock()
        kcidb_file.data = {
            'tests': [
                {'id': 'redhat:1', 'misc': {'results': [{'id': 1}, {'id': 2}]}},
                {'id': 'redhat:2', 'misc': {'results': [{'id': 3}, {'id': 4}]}}
            ]
        }

        expected_testresults = [
            {'id': 1, 'test_id': 'redhat:1'},
            {'id': 2, 'test_id': 'redhat:1'},
            {'id': 3, 'test_id': 'redhat:2'},
            {'id': 4, 'test_id': 'redhat:2'}
        ]
        add_testresults_to_kcidbfile_dict(kcidb_file)
        self.assertCountEqual(kcidb_file.data['testresults'], expected_testresults)

    def test_yesno(self):
        """Test yesno filter/function."""
        data = [
            [{'condition': True, 'values': 'yeah,no,maybe'}, 'yeah'],
            [{'condition': False, 'values': 'yeah,no,maybe'}, 'no'],
            [{'condition': None, 'values': 'yeah,no,maybe'}, 'maybe'],

            [{'condition': True, 'values': ' well done ,no,maybe'},
             ' well done '],
            [{'condition': False, 'values': 'yes, try again ,maybe'},
             ' try again '],
            [{'condition': None, 'values': 'yes,no, who knows '},
             ' who knows '],

            [{'condition': True, 'values': 'yes,no'}, 'yes'],
            [{'condition': False, 'values': 'yes,no'}, 'no'],
            [{'condition': None, 'values': 'yes,no'}, 'no'],

            [{'condition': True}, 'yes'],
            [{'condition': False}, 'no'],
            [{'condition': None}, 'no']
        ]
        for args, result in data:
            self.assertEqual(yesno(**args), result)
        self.assertRaises(IndexError, yesno, False, values='yes')
        self.assertRaises(IndexError, yesno, None, values='yes')
        self.assertRaises(IndexError, yesno, False, values='yes.no')

    def test_status_to_emoji(self):
        """Test status_to_emoji filter/function."""
        self.assertEqual(status_to_emoji('PASS'), '✅')
        self.assertEqual(status_to_emoji('FAIL'), '❌')
        self.assertEqual(status_to_emoji('ERROR'), '⚡')
        self.assertEqual(status_to_emoji('MISS'), '🕳️')
        self.assertEqual(status_to_emoji('MISSED'), '🟩')
        self.assertEqual(status_to_emoji('SKIPPED'), '⏭️')
        self.assertEqual(status_to_emoji('SKIP'), '❔')
        self.assertEqual(status_to_emoji('DONE'), '❔')

    def test_join_addresses(self):
        """Test the join_addresses function."""

        data = ['foo', '  bar', 'baz ']
        expected = 'foo, bar, baz'

        self.assertEqual(join_addresses(data), expected)

    def test_unique_test_descriptions(self):
        """Test the unique_test_descriptions filter/function."""
        unique_tests = [MagicMock(architecture='aarch64', debug=False, comment='foo'),
                        MagicMock(architecture='x86_64', debug=False, comment='abc'),
                        MagicMock(architecture='x86_64', debug=False, comment='foo'),
                        MagicMock(architecture='x86_64', debug=True, comment='foo')]

        nonunique_tests = unique_tests.copy()
        nonunique_tests += [MagicMock(architecture='x86_64', debug=False, comment='abc'),
                            MagicMock(architecture='x86_64', debug=False, comment='foo')]

        self.assertEqual(unique_test_descriptions(unique_tests), unique_tests)
        self.assertEqual(unique_test_descriptions(nonunique_tests), unique_tests)

    def test_unique_test_descriptions_sort(self):
        """Test the sorting of unique_test_descriptions filter/function."""
        unique_tests = [MagicMock(architecture='x86_64', debug=True, comment='foo'),
                        MagicMock(architecture='aarch64', debug=False, comment='foo'),
                        MagicMock(architecture='x86_64', debug=False, comment='foo'),
                        MagicMock(architecture='x86_64', debug=False, comment='abc'),
                        MagicMock(architecture='aarch64', debug=True, comment='foo')]

        # arch alphabeticaly, debug false then true, comment alphabeticaly
        unique_tests_sorted = [unique_tests[1], unique_tests[4], unique_tests[3],
                               unique_tests[2], unique_tests[0]]

        self.assertEqual(unique_test_descriptions(unique_tests), unique_tests_sorted)

    def test_get_architecture_plus_variant(self):
        """Test the get_architecture_plus_variant function."""

        test_with_variant = MagicMock(architecture='x86_64', variant='foo')
        test_without_variant = MagicMock(architecture='bar', variant=None)

        self.assertEqual(get_architecture_plus_variant(test_with_variant), "x86_64 (foo)")
        self.assertEqual(get_architecture_plus_variant(test_without_variant),
                         test_without_variant.architecture)

    def test_add_cross_references(self):
        """Test the add_cross_references function."""
        # Mocking the data structure
        checkout = mocks.get_mocked_checkout(id="rh:1", valid=True)
        build = mocks.get_mocked_build(id="rh:2", checkout_id="rh:1", valid=True)
        test = mocks.get_mocked_test(id="rh:3", build_id="rh:2", status="FAIL")
        testresult = mocks.get_mocked_test_results(id="rh:4", test_id="rh:3", status="FAIL")

        cases = [
            ("checkout", checkout, {"checkout_id": checkout.id}),
            ("build", build, {"build_id": build.id}),
            ("test", test, {"test_id": test.id}),
            ("testresult", testresult, {"testresult_id": testresult.id}),
        ]
        for description, ref_kcidb_obj, issue_occurrence_fk in cases:
            with self.subTest(description):
                # Create mocked data
                issue_occurrence = mocks.get_mocked_issue_occurrence(**issue_occurrence_fk)
                all_data = mocks.get_mocked_kcidbfile(
                    checkouts=[checkout],
                    builds=[build],
                    tests=[test],
                    results=[testresult],
                    issueoccurrences=[issue_occurrence],
                )

                with self.assertNoLogs(logger=LOGGER, level="WARNING"):
                    add_cross_references(all_data)

                # Verify that the relationships were set correctly
                self.assertEqual(checkout.builds, [build])
                self.assertEqual(build.tests, [test])
                self.assertEqual(test.testresults, [testresult])
                self.assertEqual(ref_kcidb_obj.issues_occurrences, [issue_occurrence])

                # TestResults' test's should also reference issues_occurrences
                if ref_kcidb_obj == testresult:
                    self.assertEqual(ref_kcidb_obj.test.issues_occurrences, [issue_occurrence])

    def test_add_cross_references_with_missing_parents(self):
        """Test add_cross_references with missing parent objects, ensuring warnings."""
        # Mocking the data structure
        build = mocks.get_mocked_build(id="rh:2", checkout_id="rh:91", valid=True)
        test = mocks.get_mocked_test(id="rh:3", build_id="rh:92", valid=True)
        testresult = mocks.get_mocked_test_results(id="rh:4", test_id="rh:93", status="FAIL")
        issue_occurrence = mocks.get_mocked_issue_occurrence(test_id="rh:93", status="FAIL")

        cases = [
            ("build", {"build_id": build.id}),
            ("test", {"test_id": test.id}),
            ("testresult", {"testresult_id": testresult.id}),
        ]
        for description, issue_occurrence_fk in cases:
            with self.subTest(description):
                # Create mocked data
                issue_occurrence = mocks.get_mocked_issue_occurrence(**issue_occurrence_fk)
                all_data = mocks.get_mocked_kcidbfile(
                    checkouts=[],
                    builds=[build],
                    tests=[test],
                    results=[testresult],
                    issueoccurrences=[issue_occurrence],
                )

                with self.assertLogs(logger=LOGGER, level="WARNING") as log_ctx:
                    add_cross_references(all_data)

                self.assertEqual(all_data.checkouts, [])
                self.assertEqual(all_data.builds, [])
                self.assertEqual(all_data.tests, [])
                self.assertEqual(all_data.testresults, [])

                # Ensure the warnings were triggered
                self.assertEqual(
                    [
                        f"WARNING:{LOGGER.name}:Missing parent checkout 'rh:91' in 'rh:2'",
                        f"WARNING:{LOGGER.name}:Missing parent build 'rh:92' in 'rh:3'",
                        f"WARNING:{LOGGER.name}:Missing parent test 'rh:93' in 'rh:4'",
                        f"WARNING:{LOGGER.name}:Missing KCIDB object in {issue_occurrence!r}",
                    ],
                    log_ctx.output,
                )

    def test_to_json(self):
        """Test utils.to_json()."""
        with self.subTest("Simple data"):
            data_simple = {"key": "value"}
            expected_output_simple = textwrap.dedent(
                """
                {
                  "key": "value"
                }
                """
            ).strip()
            self.assertEqual(
                to_json(data_simple),
                expected_output_simple,
            )

        with self.subTest("Nested data"):
            data_with_nested_structure = {"key": "value", "nested": {"key2": [1, 2, 3]}}
            expected_output_with_nested_structure = textwrap.dedent(
                """
                {
                  "key": "value",
                  "nested": {
                    "key2": [
                      1,
                      2,
                      3
                    ]
                  }
                }
                """
            ).strip()
            self.assertEqual(
                to_json(data_with_nested_structure),
                expected_output_with_nested_structure,
            )

    @responses.activate
    def test_get_attributes(self):
        """Test utils.get_attributes() serializes DataWarehouse-API-lib objects into dicts."""
        checkout_id = "redhat:1"
        checkout = {"id": checkout_id, "origin": "redhat", "valid": True, "misc": {"iid": 1}}
        build = {
            "id": "redhat:1-x86_64",
            "origin": "redhat",
            "checkout_id": checkout_id,
            "valid": True,
            "misc": {"iid": 2},
        }
        test = {
            "id": "redhat:1-x86_64-1",
            "origin": "redhat",
            "build_id": "redhat:1-x86_64",
            "status": "FAIL",
            "misc": {"iid": 3},
        }
        results = [
            {
                "id": "redhat:1-x86_64-1_1",
                "origin": "redhat",
                "test_id": "redhat:1-x86_64-1",
                "status": "FAIL",
            },
            {
                "id": "redhat:1-x86_64-1_2",
                "origin": "redhat",
                "test_id": "redhat:1-x86_64-1",
                "status": "ERROR",
            },
        ]
        kcidb_data = {
            "checkouts": [checkout],
            "builds": [build],
            "tests": [test],
            "testresults": results,
        }
        responses.get(f"http://localhost/api/1/kcidb/checkouts/{checkout_id}", json=checkout)
        responses.get(f"http://localhost/api/1/kcidb/checkouts/{checkout_id}/all", json=kcidb_data)
        checkout_data = CheckoutData(checkout_id)

        with self.subTest("Valid dict of KCIDBTest list with results"):
            result = get_attributes({"failures": checkout_data.test_data.untriaged_failed_tests})

            # NOTE: CheckoutData should make sure "results" are nested into their KCIDBTest
            expected_test_serialization = copy.deepcopy(test)
            expected_test_serialization["misc"] |= {"results": results}
            self.assertEqual(result, {"failures": [expected_test_serialization]})

        with self.subTest("Invalid data log exception and serialize the error message"):
            with self.assertLogs(LOGGER, level="WARNING") as log_ctx:
                bad_result = get_attributes({"failures": None})

            expected_msg = "Failed to get attributes from None"
            self.assertEqual(bad_result, {"failures": {"error": expected_msg}})
            self.assertIn(
                f"ERROR:{LOGGER.name}:{expected_msg}\nTraceback (most recent call last):\n",
                "\n".join(log_ctx.output),
            )

    def test_get_artifacts(self):
        """Test utils.get_artifacts works as expected."""
        index_prefix = (
            "https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/trusted-artifacts/"
            "1585092598/test_x86_64_debug/8619565278"
        )
        cases = {
            "empty": ([], []),
            "no match": (
                [
                    {"name": "avc.log", "url": "http://example.com/no-match/one"},
                    {"name": "dmesg.log", "url": "http://example.com/no-match/two"},
                ],
                [
                    {"name": "avc.log", "url": "http://example.com/no-match/one"},
                    {"name": "dmesg.log", "url": "http://example.com/no-match/two"},
                ],
            ),
            "match": (
                [
                    {"name": "avc.log", "url": "http://example.com/no-match/here"},
                    {
                        "name": "dmesg.log",
                        "url": (
                            f"{index_prefix}/artifacts/run.done.01/job.01/recipes/17679215/"
                            "tasks/5/results/1733984813/logs/dmesg.log"
                        ),
                    },
                ],
                [{"name": "index.html", "url": f"{index_prefix}/index.html"}],
            ),
        }
        for description, (artifacts, expected) in cases.items():
            with self.subTest(description):
                result = get_artifacts(artifacts)
                self.assertEqual(result, expected)
